Demo-project for IDEA-289595
============================

This repository demonstrates the issue described in [IDEA-289595](https://youtrack.jetbrains.com/issue/IDEA-289595).

Running `./gradlew build` works fine, but the class `TestDependencyAgain` shows
an error: `Cannot resolve symbol 'Dependency`