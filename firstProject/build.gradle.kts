plugins {
    java
    `java-test-fixtures`
}

sourceSets {
    testFixtures {
        java {
            srcDir("src/test")
        }
        resources {
            srcDir("src/test")
        }
    }
}

version = "unspecified"

repositories {
    mavenCentral()
}

dependencies {
    testFixturesApi("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}