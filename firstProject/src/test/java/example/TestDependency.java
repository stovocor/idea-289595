package example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestDependency {

    @Test
    void testGet() {
        Assertions.assertEquals(new Dependency().get(), "Dependency");
    }

}
