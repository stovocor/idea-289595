package example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestDependencyAgain {

    @Test
    void testDependencyAgain() {
        Assertions.assertEquals(new Dependency().get(), "Dependency");
    }

}
